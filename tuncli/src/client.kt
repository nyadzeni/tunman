import com.bitato.ut.api.io.streamers.StreamerFactory
import com.bitato.ut.api.io.streamers.UtStreamerFactory
import com.bitato.ut.api.model.net.http.HttpRequest
import com.bitato.ut.api.model.net.http.UtHttpStreamFactory
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.api.util.io
import com.bitato.ut.api.util.protect
import com.bitato.ut.api.util.waitWhile
import org.aeonbits.owner.Config
import org.aeonbits.owner.ConfigFactory
import java.io.IOException
import java.net.ServerSocket
import java.net.Socket
import java.net.SocketException
import java.net.URL
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


@Config.Sources("file:client.properties")
interface ClientConfiguration : Config {

    @Config.DefaultValue("8080")
    fun localPort(): Int

    @Config.DefaultValue("80")
    fun remotePort(): Int

    @Config.DefaultValue("localhost")
    fun remoteHost(): String

    @Config.DefaultValue("http://www.bing.com/")
    fun fs(): String

    @Config.DefaultValue("128")
    fun maxConnections(): Int

    @Config.DefaultValue("10000")
    fun maxHeaderSize(): Int
}

fun mockRequest(address: String, encodedData: String): String {

    val url: URL
    if (!address.startsWith("http://") && !address.startsWith("https://")) {
        url = URL("http://$address")
    } else {
        url = URL(address)
    }

    val host = url.host
    val path = url.path

    return "GET $path HTTP/1.1\r\nHost: $host\r\nConnection: Keep-Alive\r\ndata: $encodedData\r\n\r\n"
}

fun main(args: Array<String>) {
    val settings = ConfigFactory.create(ClientConfiguration::class.java)
    ClientServer(settings.localPort()).listen()
}

class ClientServer(val bindPort: Int) {

    private val threadPool: ExecutorService
    private val settings: ClientConfiguration

    init {
        settings = ConfigFactory.create(ClientConfiguration::class.java)
        threadPool = Executors.newFixedThreadPool(settings.maxConnections())
    }

    fun listen() {

        protect {
            println("Binding to port $bindPort")
            val server = ServerSocket(bindPort)

            println("Server started on port $bindPort")
            while (true) {
                protect {
                    val clientWorker = ClientWorker(server.accept())
                    threadPool.submit(clientWorker)
                }
            }
        }
    }
}

class ClientWorker(val client: Socket) : Runnable {

    private lateinit var settings: ClientConfiguration

    init {
        settings = ConfigFactory.create(ClientConfiguration::class.java)
    }

    override fun run() {

        io {
            val httpRequest = UtHttpStreamFactory.createHttpRequest(client.inputStream)

            if (isHttpRequestValid(httpRequest)) {
                if (httpRequest.method == "CONNECT") {
                    handleClientTunnel(httpRequest)
                } else {
                    handleHttpClient(httpRequest)
                }
            }
        }

        io { client.close() }
    }

    private fun isHttpRequestValid(httpRequest: HttpRequest): Boolean {
        //TODO - Add more validation
        return httpRequest.port > 0
    }

    private fun encodeClientData(data: ByteArray): ByteArray {
        return Base64.getEncoder().encode(data)
    }

    private fun handleClientTunnel(httpRequest: HttpRequest) {
        val server = Socket(settings.remoteHost(), settings.remotePort())

        val encodedBytes = encodeClientData(httpRequest.parse())

        try {
            server.outputStream < mockRequest(settings.fs(), String(encodedBytes))

            val response = UtHttpStreamFactory.createHttpResponse(server.inputStream)
            client.outputStream < response

            if (response.code == "200") {
                streamContent(server)
            }

        } catch (ex: IOException) {
        }

        server.close()
    }

    private fun streamContent(server: Socket) {
        val serverToClient = Thread {

            val streamer = UtStreamerFactory().create(server.inputStream, client.outputStream, StreamerFactory.StreamerType.INDEFINITE)
            try {
                streamer.stream()
            } catch (ex: SocketException) {
            }
        }

        val clientToServer = Thread {
            try {
                val buffer = ByteArray(settings.maxHeaderSize())
                var curBufferSize = client.inputStream.read(buffer)

                while (curBufferSize != -1) {
                    if (curBufferSize > 0) {
                        val encodedData = encodeClientData(Arrays.copyOf(buffer, curBufferSize))

                        server.outputStream < mockRequest(settings.fs(), String(encodedData))
                        server.outputStream.flush()
                    }

                    curBufferSize = client.inputStream.read(buffer)
                }
            } catch (ex: SocketException) {
            }
        }

        serverToClient.start()
        clientToServer.start()

        protect { waitWhile { serverToClient.isAlive && clientToServer.isAlive } }

        if (serverToClient.isAlive) {
            serverToClient.interrupt()
        }

        if (clientToServer.isAlive) {
            clientToServer.interrupt()
        }
    }

    private fun handleHttpClient(httpRequest: com.bitato.ut.api.model.net.http.HttpRequest) {
        val server = Socket(settings.remoteHost(), settings.remotePort())
        val encodedRequest = encodeClientData(httpRequest.parse())

        io {
            server.outputStream < mockRequest(settings.fs(), String(encodedRequest))
            streamContent(server)
        }

        server.close()
    }
}