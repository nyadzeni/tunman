import com.bitato.ut.api.io.streamers.StreamerFactory
import com.bitato.ut.api.io.streamers.UtStreamerFactory
import com.bitato.ut.api.model.net.http.HttpRequest
import com.bitato.ut.api.model.net.http.UtHttpStreamFactory
import com.bitato.ut.api.util.compareTo
import com.bitato.ut.api.util.io
import com.bitato.ut.api.util.protect
import com.bitato.ut.api.util.waitWhile
import java.io.ByteArrayInputStream
import java.io.IOException
import java.net.ServerSocket
import java.net.Socket
import java.net.SocketException
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


fun decryptData(data: String): ByteArray {
    return Base64.getDecoder().decode(data)
}

fun main(args: Array<String>) {

    var bindPort = -1
    if (args.size > 0) {
        try {
            bindPort = args[0].toInt()

            if (bindPort <= 0 || bindPort > 65535) {
                bindPort = -1
            }

        } catch (ex: NumberFormatException) {
        }
    }

    if (bindPort > 0)
        RemoteServer(bindPort).listen()
    else
        println("Usage: tunserv [port]")
}

class RemoteServer(val bindPort: Int) {

    private val threadPool: ExecutorService

    init {
        threadPool = Executors.newFixedThreadPool(256)
    }

    fun listen() {

        protect {
            val server = ServerSocket(bindPort)
            println("Server started on port $bindPort")

            while (true) {
                protect {
                    val serverWorker = ServerWorker(server.accept())
                    threadPool.submit(serverWorker)
                }
            }
        }
    }
}

class ServerWorker(val client: Socket) : Runnable {

    override fun run() {

        io {
            val dummyRequest = UtHttpStreamFactory.createHttpRequest(client.inputStream)

            if (!dummyRequest.headers.containsHeaderWithName("data")) {
                return@io
            }

            val encodedData = dummyRequest.headers.getValueOf("data")
            val realRequest = UtHttpStreamFactory.createHttpRequest(ByteArrayInputStream(decryptData(encodedData)))

            if (isHttpRequestValid(realRequest)) {
                if (realRequest.method == "CONNECT") {
                    handleTunnelClient(realRequest)
                } else {
                    handleHttpClient(realRequest)
                }
            }
        }

        io { client.close() }
    }

    private fun isHttpRequestValid(realRequest: HttpRequest): Boolean {
        //TODO
        return realRequest.port > 0
    }

    private fun handleTunnelClient(httpRequest: HttpRequest) {
        val remote = Socket(httpRequest.getHost(), httpRequest.port)
        client.outputStream < "HTTP/1.1 200 Connection Established\r\nConnection: Keep-Alive\r\n\r\n"

        io {
            streamContent(remote)
        }

        remote.close()
    }

    private fun handleHttpClient(httpRequest: HttpRequest) {

        val remote = Socket(httpRequest.getHost(), httpRequest.port)

        io {
            remote.outputStream < httpRequest
            streamContent(remote)
        }

        remote.close()
    }

    private fun streamContent(remote: Socket) {
        val serverToClient = object : Thread() {
            override fun run() {
                val streamer = UtStreamerFactory().create(remote.inputStream, client.outputStream, StreamerFactory.StreamerType.INDEFINITE)
                try {
                    streamer.stream()
                } catch (ex: SocketException) {
                }
            }
        }

        val clientToServer = object : Thread() {
            override fun run() {
                try {
                    while (true) {
                        val dummyRequest = UtHttpStreamFactory.createHttpRequest(client.inputStream)
                        val encodedData = dummyRequest.headers.getValueOf("data")
                        remote.outputStream < decryptData(encodedData)
                    }
                } catch (ex: IOException) {
                }
            }
        }

        serverToClient.start()
        clientToServer.start()

        protect { waitWhile { serverToClient.isAlive && clientToServer.isAlive } }
    }

}